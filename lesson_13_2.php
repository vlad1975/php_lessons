<!DOCTYPE html>
<html>
<head>
    <title>Lesson 13.2</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 13.2</h2><hr/>
    <?php
    $fname = 'content.txt';
    if (!empty($_GET['text'])) {
    	file_put_contents($fname, $_GET['text']);
    }
    ?>

    <form>
    	<textarea name="text" cols="40" rows="10"><?php
    		if (file_exists($fname)) {
    			echo(htmlspecialchars(file_get_contents($fname)));
    		}
    		?></textarea> 
    	<br>
    	<input type="submit" value="Save">
    </form>
    <br>
</body>
</html>