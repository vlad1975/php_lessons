<!DOCTYPE html>
<html>
<head>
    <title>Lesson 10.5 (arrays)</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 10.5 (arrays)</h2><hr/>
    
    <?php
    $start = microtime(true);
    for ($i=0; $i < rand(5,15); $i++) { 
    	$arr[] = rand(0, 1000);
    }
    echo('<pre>');
    print_r($arr);

    
    for ($i=0; $i < count($arr); $i++) { 
      	$min = $arr[$i];
      	for ($j=$i; $j < count($arr); $j++) { 
      		if ($arr[$j]<$min) {
      			list($arr[$i], $arr[$j]) = array($arr[$j], $arr[$i]);
      			$min = $arr[$i];
      		}
      	}
    }
    //sort($arr);
    print_r($arr);
   
    echo('</pre>');
    $stop = microtime(true);
    echo "The execution time of the PHP script is : ".($stop - $start)." sec";
    ?>
</body>
</html>