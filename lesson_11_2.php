<!DOCTYPE html>
<html>
<head>
    <title>Lesson 11.2</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 11.2</h2><hr/>
	<form>
		<label for="number">Number</label>
		<input type="number" name="number">
	</form>
    <?php
    function odd($n)
    {
    	return (bool)($n % 2);
    }
    var_dump(odd($_GET['number']));
    ?>
</body>
</html>
 