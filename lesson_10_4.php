<!DOCTYPE html>
<html>
<head>
    <title>Lesson 10.4</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 10.4</h2><hr/>
    
    <form>
    	<label for="number1">Number 1</label>
    	<input type="number" name="number1" min="1">
    	<label for="number2">Number 2</label>
    	<input type="number" name="number2" min="1">
    	<input type="submit">
    	<br><br>
    </form>
    <?php
    if (count($_GET) == 0) exit('Enter numbers');
    $a = $_GET['number1']; $b = $_GET['number2'];
    echo "\nThe number before swapping is:\n";
	echo "Number a =".$a." and b=".$b."<br>";

	extract(array('a' => $b, 'b' => $a ));

	echo "\nThe number after swapping is: \n";
	echo "Number a =".$a." and b=".$b."\n";
    ?>
</body>
</html>