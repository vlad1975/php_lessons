<!DOCTYPE html>
<html>
<head>
    <title>Lesson 8.2</title>
     <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 8.2 (create file with date name and random filled)</h2><hr/>
    <?php //rnd file
    $fname = date("Y-m-d-H-i-s").".txt";
    $data = rand();
    if (file_put_contents($fname, $data))
    {
        echo "Ok! File name: $fname, data: $data (".getrandmax().")";
    }
    else
        echo "Something wrong ";
    ?>
</body>
</html>