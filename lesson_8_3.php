<!DOCTYPE html>
<html>
<head>
    <title>Lesson 8.3</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 8.3 (Extensions and constants)</h2><hr/>
    <?php
    /* Need write two files:
    1. All available extensions
    2. All predefined PHP constants
    --------------------------------
    */
    //1. Extensions

    writeFile("extensions.txt", get_loaded_extensions()); 
    
    // 2. Predefined Constants
    writeFile("constants.txt",get_defined_constants());

    function writeFile($fname,$arr)
    {
        $data = "";
        foreach ($arr as $key => $value)
            $data = $data.sprintf("%-'.60s", $key).$value."\n";
        if (file_put_contents ($fname, $data)) 
        {    
            echo "File '$fname' created!<br>";
        } 
        else 
        {    
            echo "Can not create file :(<br>"; 
        }
            return 0;
    }
    ?> 
</body>
</html>