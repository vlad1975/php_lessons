<?php

if (isset($_COOKIE['name'])) {
	greet($_COOKIE['name']);
	exit();	
}

if (isset($_GET['name'])){
	setcookie('name',$_GET['name']);
	exit();
}

function greet($n){
	echo('Time: '.date('H:i').'<br>');
	switch (true) {
		case (5 < date('H') && date('H') <= 11):
			echo ('Good morning, '.$n.'!');
			break;
		case (11 < date('H') && date('H') <= 16):
			echo ('Good afternoon, '.$n.'!');
			break;
		case (16 < date('H') && date('H') <= 24):
			echo ('Good evening, '.$n.'!');
			break;
		case (0 < date('H') && date('H') <= 5):
			echo ('Good night, '.$n.'!');
			break;
		}
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Lesson 14.1</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 14.1</h2><hr/>
    <form>
    	<label for ="name">Name: </label>
    	<input type="text" name="name">
    	<input type="submit">
    </form>

</body>
</html>