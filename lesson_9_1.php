<!DOCTYPE html>
<html>
<head>
    <title>Lesson 9.1</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 9.1 (Fibonacci number)</h2><hr/>
    <form action=" ">
    	<label for="number">Number (3 and greater)</label>
    	<input type="number" name="number" min="1">
    	<input type="submit">
    </form><br>
    <?php
    if (!count($_GET)) exit('Enter number, please');

    //Method 1:
    $n = (int) $_GET['number'];
    $a = $b = 1;
    for ($i=3; $i <=$n ; $i++) { 
    	list ($a, $b) = array($b,$a+$b);
    }
    echo "Method 1: F($n) = $b<br>";

   	//Method 2:
   	function fib($x){
   		if ($x <= 2) {
   			# code...
   			return 1;
   		}
   		else {
   			return fib($x-1) + fib ($x-2);
   		}
   	}
   	echo ("Method 2: F($n) = ".fib($n));
    ?>
</body>
</html>