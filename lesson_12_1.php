<!DOCTYPE html>
<html>
<head>
    <title>Lesson 12.1</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 12.1</h2><hr/>
    <?php
    function highlight_file_my($f){
    	$s = file($f);
    	foreach ($s as $value) {
    		highlight_string($value);
    	}
    }
    
    echo('highlight_string(str): ');
    highlight_string('<?php phpinfo(); ?>');
    echo('<br> <br>highlight_file(filename):<br>');
    highlight_file_my('./lesson_12_1.php');


    ?>
</body>
</html>