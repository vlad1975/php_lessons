<!DOCTYPE html>
<html>
<head>
    <title>Lesson 10.6</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 10.6</h2><hr/>
    
    <?php
    $fname = 'month.txt';
    $arr = file('./'.$fname);
    echo "File: $fname: <br><br>";
    foreach ($arr as $k => $v) {
    	echo("<b>".($k+1).".</b>  $v<br>");
    }
    ?>
</body>
</html>