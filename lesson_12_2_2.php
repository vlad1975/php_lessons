<!DOCTYPE html>
<html>
<head>
    <title>Lesson 12.2.2</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 12.2 get object</h2><hr/>
    <?php
    require_once './class/ClassPoint.php';
    $point = new ClassPoint;
    $fname = 'point.txt';
    $s = file_get_contents($fname);
    if (!$s) exit('Can not read the file');
    echo("<pre>");
    $point = unserialize($s);
    print_r($point);
    echo($point->x);
    echo("</pre>");
    ?>
</body>
</html>