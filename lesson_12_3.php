<!DOCTYPE html>
<html>
<head>
    <title>Lesson 12.3</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 12.3</h2><hr/>
    <form>
    	<label for="number"> Arab number (1 - 3999): </label>
    	<input type="number" name="number" min="1" max="3999">
    	<input type="submit" value="Convert">
    </form>
    <?php
    if (!$_GET['number']) exit ('Enter number, please');
    $a = array(
    	array(
    		array('0','1','2','3'),
    		array('','M','MM','MMM')
    	),
    	array(
    		array('0','1','2','3','4','5','6','7','8','9'),
    		array('','C','CC','CCC','CD','D','DC','DCC','DCCC','CM')
    	),
    	array(
    		array('0','1','2','3','4','5','6','7','8','9'),
    		array('','X','XX','XXX','XL','L','LX','LXX','LXXX','XC')
    	),
    	array(
    		array('0','1','2','3','4','5','6','7','8','9'),
    		array('','I','II','III','IV','V','VI','VII','VIII','IX')
    	)
    );
    $num = sprintf('%04d', $_GET['number']);
    
    for ($i=0; $i < 4; $i++) $s[] = str_replace($a[$i][0], $a[$i][1], substr($num,$i, 1));

    echo('Roman numerals: '.implode('', $s));
    ?>
</body>
</html>