<!DOCTYPE html>
<html>
<head>
    <title>Lesson 12.2</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 12.2</h2><hr/>
    <?php
    require_once './class/ClassPoint.php';
    $point = new ClassPoint;
    $point->x = 1;
    $point->y = 1;
    $fname = 'point.txt';
    if (!file_put_contents($fname, serialize($point))) exit('Something wrong');
    echo("File $fname created<br>");
    ?>
</body>
</html>