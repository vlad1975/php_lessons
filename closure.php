<?php
echo "<h2> Closure </h2><br>";

//$c = 10;

$f = function($a,$b) use ($c){
    return $a + $b + $c;
};

echo "$c<br>";
$c = 20;
echo "$c<br>";
$c = 30;
echo "$c<br>";
$c = 40;
echo "$c<br>";
$c = 50;
echo "$c<br>";
$c = 60;
echo "$c<br>";

echo $f(1,2);
?>