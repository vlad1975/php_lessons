<!DOCTYPE html>
<html>
<head>
    <title>Lesson 9.2 (calendar)</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 9.2 (calendar)</h2><hr/>
    <?php
    $days = array('Mon','Tue','Wed','Thu','Fri','Sat','Sun');
    $daysnumber = cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y'));
    $firstday = date('N', strtotime("01-".date('m')."-".date('Y')));
    $f = 0;
    $count = 2;

    echo "<table><caption>".date('F')."</caption>";
    
    for ($i=0; $i <7 ; $i++) { 
    	echo "<th>".$days[$i]."</th>";
    }
    echo("</tr>");

    while ( $count< $daysnumber) {
       	echo("<tr>");
    	for ($j=0; $j <7 ; $j++) {
    		if (!$f) {
    			if ($j+1==$firstday) {
    				$f = 1;
    				echo("<td>"."1"."</td>");
    			}
    			else echo("<td>"." "."</td>");
    		}
    		else echo("<td>".($count<=$daysnumber?$count++:" ")."</td>");
    	}
    	echo("</tr>");
    }
    echo("</table>");    
    ?>
</body>
</html>