<!DOCTYPE html>
<html>
<head>
    <title>Lesson 8.1.2</title>
     <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 8.1.2 (read the file)</h2><hr/>
    <?php
    $fname = "hello.txt";
    if (file_exists($fname))
    {
        $data = file_get_contents($fname);
        echo "$fname -> ".$data;
    }
    else
        echo "File don't exists!";
    ?>
</body>
</html>