<!DOCTYPE html>
<html>

<head>
    <title>Lesson 15.2</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>

<body>
    <?php

    while (true) {
        
        if (empty($_GET)) break;
        $number = test($_GET['number']);
        $price = test($_GET['price']);
        $f_number = filter_var($number, FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]]);
        
        if (!$f_number) $err[] = 'Number is not valid';

        $f_price = filter_var($price, FILTER_VALIDATE_REGEXP, ['options' => ['regexp' => '/^\d{1,3}[.]\d{2}$/']]);

        if (!$f_price) $err[] = 'Price format is not valid.';

        break;
    }
    ?>
    <h2>Lesson 15.2</h2>
    <hr />
    <form>
        <table>
            <tr>
                <td>Number (integer more than 0):</td>
                <td><input type="text" name="number" id="" value="<?= $number ?>"></td>
            </tr>
            <tr>
                <td>Price (format ###.##):</td>
                <td><input type="text" name="price" id="" value="<?= $price ?>"></td>
            </tr>
            <tr>
                <td><input type="submit" value="Send"></td>
            </tr>
        </table>

    </form>
    <?php

    if (!empty($err)) {
        foreach ($err as $msg) echo "<span style = 'color:red;'> $msg </span><br>";
    } elseif (!empty($_GET)) {
        echo "<span style = 'color:green;'> Your total price:&nbsp".($f_price*$f_number)."</span><br>";
        echo "Thank you.";
    } 

    function test($data)
    {
        return strip_tags(trim($data));
    }

    ?>
</body>

</html>