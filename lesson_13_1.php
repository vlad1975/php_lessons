<!DOCTYPE html>
<html>
<head>
    <title>Lesson 13.1</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 13.1</h2><hr/>

    <form>
    	<fieldset width ="230px">
    		<legend>Distance:</legend>
	    	<label for="x1">X1: </label>
	    	<input type="number" name="x1" value="<?= $_GET['x1']?>">
	    	<label for="y1">Y1: </label>
	    	<input type="number" name="y1" value="<?= $_GET['y1']?>">
	    	<br>
	    	<label for="x2">X2: </label>
	    	<input type="number" name="x2" value="<?= $_GET['x2']?>">
	    	<label for="y2">Y2: </label>
	    	<input type="number" name="y2" value="<?= $_GET['y2']?>">
	    	<br>
	    	<input type="submit" value="calculate">
    	</fieldset>
    </form>
    <br>
    <br>
    <?php
    if (empty($_GET)) exit();

    require_once './class/ClassPoint.php';
    $p1 = new ClassPoint;
    $p2 = new ClassPoint;
    
    $p1->x = $_GET['x1'];
    $p1->y = $_GET['y1'];

    $p2->x = $_GET['x2'];
    $p2->y = $_GET['y2'];
    
    echo "Two points distance: ".sqrt(pow(($p2->x - $p1->x),2) + pow(($p2->y - $p1->y),2));
    ?>
</body>
</html>