<!DOCTYPE html>
<html>

<head>
    <title>Lesson 15.1</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>

<body>
    <h2>Lesson 15.1</h2>
    <hr />
    <?php

    while (true) {
        if (empty($_GET))  break; //Check is page new before submit

        $fname = 'users.txt';
        $err = array();
        $data = array(
            'name' => test($_GET['name']),
            'fam' => test($_GET['fam']),
            'email' => test($_GET['email'])
        );

        //Data validation----------------------------------------------------------------
        if (empty($data['name'])) $err['name'] =  'Must be!';
        elseif (!filter_var($data['name'], FILTER_CALLBACK, array('options' => function ($v) {
            return preg_match('/^[a-z]+$/ui', $v);
        }))) $err['name'] = 'English letters only!';

        if (empty($data['fam'])) $err['fam'] =  'Must be!';
        elseif (!filter_var($data['fam'], FILTER_CALLBACK, array('options' => function ($v) {
            return preg_match('/^[a-z]+$/ui', $v);
        }))) $err['fam'] = 'English letters only!';

        if (empty($data['email'])) $err['email'] =  'Must be!';
        elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) $err['email'] = 'Not valid E-mail';

        if (empty($err['name']) && empty($err['fam']) && empty($err['email'])) $err['global'] = "ok";
        else $err['global'] = "fix";

        if ($err['global'] == 'ok') {

            if (file_exists($fname)) $users = file($fname);
            else $users = [];  //Read data
            
            $user = sprintf('%-15s %-15s %-25s', $data['name'], $data['fam'], $data['email']) . PHP_EOL;

            if (!empty($users) && !(array_search($user, $users) === false)) {
                $err['global'] = 'exists';
            } else {
                if (file_put_contents($fname, $user, FILE_APPEND)) $err['global'] = 'created';
            }
        }

        break;
    }
    ?>
    <form method="GET">
        <span style="color: red; font-size: 70%;"> * Must be</span>
        <table>
            <tr>
                <td>First Name:</td>
                <td><input type="text" name="name" value="<?= $data['name'] ?>"></td>
                <td><span style="color: red; font-size: 70%;">*<?= $err['name'] ?></span></td>
            </tr>
            <tr>
                <td>Second Name:</td>
                <td><input type="text" name="fam" value="<?= $data['fam'] ?>"></td>
                <td><span style="color: red; font-size: 70%;">*<?= $err['fam'] ?></span></td>
            </tr>
            <tr>
                <td>E-mail:</td>
                <td><input type="text" name="email" value="<?= $data['email'] ?>"></td>
                <td><span style="color: red; font-size: 70%;">*<?= $err['email'] ?></span></td>
            </tr>
        </table>
        <input type="submit">
    </form>
    <?php

    if ($err['global'] == 'fix') echo "<br><span style='color: red;'>Fix problems please!</span>";
    elseif ($err['global'] == 'exists') echo "<br><span style='color: red;'>User exists</span>";
    elseif ($err['global'] == 'created') echo "<br><span style='color: green;'>User created</span>";

    function test($data)
    {
        return trim(strip_tags($data));
    }
    ?>
</body>

</html>