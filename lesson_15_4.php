<!DOCTYPE html>
<html>

<head>
    <title>Lesson 15.4</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>

<body>
    <h2>Lesson 15.4</h2>
    <hr />
    <?php

    $text = 'На рис. 6 представлен график по данным табл. 8. Как видно из рисунка, максимум приходится на 2002 г., поэтому целесообразно сосредоточиться на периоде 1998-2002 гг., как наиболее показательном.';

    $new_text = preg_replace("/(?<=(?<!г)(?<!рис)(?<!табл))\./", "...", $text);
    echo $text.'<hr>';
    echo $new_text;
    //comment
    ?>

</body>

</html>
