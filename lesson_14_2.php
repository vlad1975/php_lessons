<!DOCTYPE html>
<html>
<head>
    <title>Lesson 14.2</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 14.2</h2><hr/>
    <?php
    echo "<pre>";
    print_r($_ENV);
    echo "</pre>";
    echo "<hr>";
    echo getenv("USER");
    $_ENV["HELLO"]="world";
    echo "<br>HELLO:".$_ENV['HELLO'];
    echo "<pre>";
    print_r($_ENV);
    echo "</pre>";
    ?>
</body>
</html>