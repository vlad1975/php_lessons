<!DOCTYPE html>
<html>
<head>
    <title>Lesson 14.5</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 14.5</h2><hr/>
    <?php
    $fname = 'ips.txt';
    $addr = $_SERVER["REMOTE_ADDR"]; //Get address
    if (file_exists($fname)) $ips = file($fname);
    else $ips=array();
    
    //write file to formatted array $i
    foreach ($ips as $key => $item) {
        $ip = trim(substr($item, 0, 15));
        //echo $ip." ".strlen($ip)."<br>";
        $num = (int) trim(substr($item,17));
        //echo $num." ".strlen($num)."<br>";
        $i[$ip] = $num;
    }
    
    // find IP address in array
    if (isset($i) && array_key_exists($addr,$i)) $i[$addr]++; //if exists add number
    else    $i[$addr] = 1;                       //Else adding IP and number visiting 1
    
    //format array data to new array $new
    foreach ($i as $key => $value) {        
        $v = sprintf('%-16s',$key).": ".$value."\n"; //left align, width 16, string
        $new[] = $v;
    }

    echo "Visitings:<br>";
    foreach ($new as $key => $item) {
        echo "<pre style='display:inline'>".trim($item)."</pre><br>";
    }
    
    //Write array $new to file
    if (!file_put_contents($fname, $new)) echo "Can't write file!";
    ?>
</body>
</html>