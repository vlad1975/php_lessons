<!DOCTYPE html>
<html lang="">
<head>
    <title>Lesson 10.1 (arrays)</title>
    <link rel="stylesheet" type="text/css" href='style.css' />
</head>
<body>
    <h2>Lesson 10.1 (arrays)</h2><hr/>
    
    <?php
    $arr = array('fst','snd','thd','fth');
    echo('<pre>');
    print_r($arr);
    $n = rand(0, count($arr)-1);
    echo("Random array number $n => [$arr[$n]]");
    echo('</pre>');
    ?>
</body>
</html>